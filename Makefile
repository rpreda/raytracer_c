NAME = RTv1
SRC = *.c \
	  ./shapes/cone.c\
	  ./shapes/cylinder.c\
	  ./shapes/planes.c\
	  ./shapes/spheres.c\
	  ./general/utils.c\
	  ./general/utils2.c\
	  ./general/vector_functions.c\
	  ./general/vector_functions2.c
INCLUDES = ./includes/
CFLAGS = -Wall -Wextra -Ofast
MLX_FLAGS = -I./minilibx/ -L./minilibx/ -lmlx -framework OpenGL -framework AppKit
all: $(NAME)

$(NAME):
			@echo "Building MLX"
			@make -C ./minilibx/
			@echo "Compiling..."
			@cc -I$(INCLUDES) $(CFLAGS) $(MLX_FLAGS) $(SRC) -o $(NAME)
clean:
	@make -C ./minilibx/ clean
	@echo "Removing executable..."
	@rm -f $(NAME)
re: clean all

