/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpreda <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/08 16:14:41 by rpreda            #+#    #+#             */
/*   Updated: 2017/12/08 16:16:24 by rpreda           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

int		key_hook(int keycode, void *scrap)
{
	(void)scrap;
	if (keycode == 53)
		exit(0);
	return (0);
}

t_vec3d	compute_cam_ray(int x, int y)
{
	t_vec3d ret_val;

	ret_val.z = -1;
	ret_val.x = ((2 * (x + 0.5)) / (float)WIDTH - 1) * tan(M_PI / 8)
	* WIDTH / (float)HEIGHT;
	ret_val.y = (1 - 2 * (y + 0.5) / (float)HEIGHT) * tan(M_PI / 8);
	return (ret_val);
}

int		expose_hook(void *env)
{
	t_env *ev;

	ev = (t_env *)env;
	mlx_put_image_to_window(ev->mlx, ev->win, ev->image, 0, 0);
	return (0);
}

void	raytrace(t_env *env)
{
	int			x;
	int			y;
	t_vec3d		ray;
	t_tracerez	res;

	x = 0;
	while (x < WIDTH)
	{
		y = 0;
		while (y < HEIGHT)
		{
			ray = normalize(compute_cam_ray(x, y));
			res = loop(env, ray, env->origin, 1);
			if (!res.no_hit)
				put_pixel(env, res.color, x, y);
			y++;
		}
		x++;
	}
	mlx_put_image_to_window(env->mlx, env->win,
		env->image, 0, 0);
}

int		main(void)
{
	t_env env;

	define_environment(&env);
	env.mlx = mlx_init();
	env.win = mlx_new_window(env.mlx, WIDTH, HEIGHT, "RTV1");
	mlx_key_hook(env.win, key_hook, 0);
	mlx_expose_hook(env.win, expose_hook, &env);
	env.image = mlx_new_image(env.mlx, WIDTH, HEIGHT);
	env.img = mlx_get_data_addr(env.image, &env.bpp, &env.ln_size, &env.endian);
	raytrace(&env);
	mlx_loop(env.mlx);
	return (0);
}
