/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpreda <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/08 16:01:37 by rpreda            #+#    #+#             */
/*   Updated: 2017/12/08 16:36:54 by rpreda           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

t_twovec	pack(t_vec3d normal, t_vec3d phit)
{
	t_twovec ret_val;

	ret_val.phit = phit;
	ret_val.normal = normal;
	return (ret_val);
}

t_raydir	pack2(t_vec3d ray, t_vec3d orig)
{
	t_raydir ret_val;

	ret_val.r = ray;
	ret_val.o = orig;
	return (ret_val);
}

void		put_pixel(t_env *env, t_color col, int x, int y)
{
	int		offset;

	offset = x * env->bpp / 8;
	offset += y * env->ln_size;
	*(env->img + offset) = (unsigned char)
		(clamp(0, col.b * 255, 254));
	*(env->img + offset + 1) = (unsigned char)
		(clamp(0, col.g * 255, 254));
	*(env->img + offset + 2) = (unsigned char)
		(clamp(0, col.r * 255, 254));
}
