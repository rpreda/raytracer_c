/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_functions2.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpreda <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/08 17:07:40 by rpreda            #+#    #+#             */
/*   Updated: 2017/12/08 17:08:38 by rpreda           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

double		magnitude(t_vec3d a)
{
	return (sqrtf(a.x * a.x + a.y * a.y + a.z * a.z));
}

t_vec3d		scale(t_vec3d a, double num)
{
	t_vec3d ret_val;

	ret_val.x = a.x * num;
	ret_val.y = a.y * num;
	ret_val.z = a.z * num;
	return (ret_val);
}
