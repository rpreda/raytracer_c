/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_functions.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpreda <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/08 17:06:03 by rpreda            #+#    #+#             */
/*   Updated: 2017/12/08 17:08:22 by rpreda           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

t_vec3d		normalize(t_vec3d a)
{
	float mag;

	mag = magnitude(a);
	a.x /= mag;
	a.y /= mag;
	a.z /= mag;
	return (a);
}

double		dot_product(t_vec3d a, t_vec3d b)
{
	return (a.x * b.x + a.y * b.y + a.z * b.z);
}

t_vec3d		cross_product(t_vec3d a, t_vec3d b)
{
	t_vec3d ret_val;

	ret_val.x = a.y * b.z - a.z * b.y;
	ret_val.y = a.z * b.x - a.x * b.z;
	ret_val.z = a.x * b.y - a.y * b.x;
	return (ret_val);
}

t_vec3d		add(t_vec3d a, t_vec3d b)
{
	t_vec3d ret_val;

	ret_val.x = a.x + b.x;
	ret_val.y = a.y + b.y;
	ret_val.z = a.z + b.z;
	return (ret_val);
}

t_vec3d		subtract(t_vec3d a, t_vec3d b)
{
	t_vec3d ret_val;

	ret_val.x = a.x - b.x;
	ret_val.y = a.y - b.y;
	ret_val.z = a.z - b.z;
	return (ret_val);
}
