/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpreda <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/08 17:04:36 by rpreda            #+#    #+#             */
/*   Updated: 2017/12/08 17:05:38 by rpreda           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

float			max(float a, float b)
{
	if (a > b)
		return (a);
	return (b);
}

float			min_abs(float a, float b)
{
	if (fabs(a) > fabs(b))
		return (b);
	return (a);
}

float			min(float a, float b)
{
	if (a > b)
		return (b);
	return (a);
}

float			clamp(float min, float val, float max)
{
	if (val > max)
		return (max);
	if (val < min)
		return (min);
	return (val);
}

t_quadratic_res	solve_quadratic(t_quadratic_coef coef)
{
	t_quadratic_res		ret_val;
	double				delta;
	double				q;

	ret_val.count = 0;
	delta = coef.b * coef.b - 4 * coef.a * coef.c;
	if (delta > 0)
	{
		if (coef.b > 0)
			q = -0.5 * (coef.b + sqrt(delta));
		else
			q = -0.5 * (coef.b - sqrt(delta));
		ret_val.count = 2;
		ret_val.x1 = q / coef.a;
		ret_val.x2 = coef.c / q;
	}
	else if (delta == 0)
	{
		ret_val.x1 = -0.5 * coef.b / coef.a;
		ret_val.x2 = ret_val.x1;
		ret_val.count = 1;
	}
	return (ret_val);
}
