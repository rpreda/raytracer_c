/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shadows.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpreda <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/08 16:08:47 by rpreda            #+#    #+#             */
/*   Updated: 2017/12/08 16:09:20 by rpreda           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

int	is_shadowed(t_env *env, t_vec3d phit, t_vec3d dir)
{
	t_tracerez trez;

	trez.z_value = 100;
	trez = loop(env, dir, phit, 0);
	if (trez.no_hit || trez.z_value < 0)
		return (0);
	return (1);
}
