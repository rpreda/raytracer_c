/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lighting.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpreda <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/08 15:07:58 by rpreda            #+#    #+#             */
/*   Updated: 2017/12/08 16:04:14 by rpreda           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

t_color	get_diffuse(t_light light, t_vec3d normal, t_vec3d phit, t_color color)
{
	t_color	ret_val;
	float	coef;
	t_vec3d light_ray;

	light_ray = normalize(subtract(light.position, phit));
	normal = normalize(normal);
	coef = max(0, dot_product(light_ray, normal));
	ret_val.r = color.r * coef * light.intensity.r;
	ret_val.g = color.g * coef * light.intensity.g;
	ret_val.b = color.b * coef * light.intensity.b;
	return (ret_val);
}

t_color	get_ambient(t_color col_orig)
{
	t_color ret_val;

	ret_val.r = col_orig.r * AMBIENT;
	ret_val.g = col_orig.g * AMBIENT;
	ret_val.b = col_orig.b * AMBIENT;
	return (ret_val);
}

t_color	get_specular(t_vec3d ray, t_light light, t_vec3d normal, t_vec3d phit)
{
	t_color	ret_val;
	t_vec3d	reflection;
	t_vec3d	light_ray;
	double	spec;

	light_ray = normalize(subtract(light.position, phit));
	reflection = normalize(subtract(scale(normal,
		dot_product(light_ray, normal) * 2), light_ray));
	ray = scale(normalize(ray), -1);
	spec = pow(clamp(0, dot_product(reflection, ray), M_PI / 2), 20);
	ret_val.r = light.intensity.r * spec;
	ret_val.g = light.intensity.g * spec;
	ret_val.b = light.intensity.b * spec;
	return (ret_val);
}

t_color	combine(t_color diff, t_color amb, t_color spec)
{
	t_color ret_val;

	ret_val.r = K_DIFF * diff.r + K_SPEC * spec.r + amb.r;
	ret_val.g = K_DIFF * diff.g + K_SPEC * spec.g + amb.g;
	ret_val.b = K_DIFF * diff.b + K_SPEC * spec.b + amb.b;
	return (ret_val);
}

t_color	ph_light(t_env *env, t_twovec nrm_phit, t_color color, t_vec3d ray)
{
	t_light temp;
	t_vec3d	phit;
	t_vec3d	normal;

	normal = nrm_phit.normal;
	phit = nrm_phit.phit;
	temp = env->lights[0];
	if (is_shadowed(env, phit,
		subtract(temp.position, phit)))
		return (get_ambient(color));
	return (combine(get_diffuse(temp, normal, phit, color),
		get_ambient(color),
		get_specular(ray, temp, normal, phit)));
}
