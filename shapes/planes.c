/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   planes.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpreda <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/08 16:57:07 by rpreda            #+#    #+#             */
/*   Updated: 2017/12/08 16:58:54 by rpreda           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

t_color		compute_plane_col(t_env *env, t_vec3d ray,
	t_plane pl, double t)
{
	t_vec3d phit;

	phit = scale(ray, t);
	if (pl.normal.y < 0)
		pl.normal.y *= -1;
	return (ph_light(env, pack(pl.normal, phit), pl.color, ray));
}

t_tracerez	plane_intersect(t_env *env, t_raydir v, t_plane plane, int do_col)
{
	double		t;
	double		denom;
	t_vec3d		p0l0;
	t_tracerez	ret_val;

	ret_val.no_hit = 1;
	plane.normal = normalize(plane.normal);
	v.r = normalize(v.r);
	denom = dot_product(plane.normal, v.r);
	if (denom > 1e-3)
	{
		p0l0 = subtract(plane.p, v.o);
		t = dot_product(p0l0, plane.normal) / denom;
		if (t >= 0)
		{
			if (do_col)
				ret_val.color = compute_plane_col(env, v.r, plane, t);
			ret_val.no_hit = 0;
			ret_val.z_value = t;
		}
	}
	return (ret_val);
}
