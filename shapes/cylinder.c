/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cylinder.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpreda <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/08 16:56:54 by rpreda            #+#    #+#             */
/*   Updated: 2017/12/08 16:56:56 by rpreda           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

t_color		compute_cyl_col(t_env *env, t_vec3d ray, t_cyl cyl, double t)
{
	t_vec3d phit;
	t_vec3d aux_perp;
	t_vec3d orig_phit;
	t_vec3d normal;

	phit = scale(ray, t);
	orig_phit = subtract(cyl.org, phit);
	orig_phit = normalize(orig_phit);
	aux_perp = cross_product(cyl.dir, orig_phit);
	aux_perp = normalize(aux_perp);
	normal = cross_product(cyl.dir, aux_perp);
	return (ph_light(env, pack(normal, phit), cyl.color, ray));
}

t_tracerez	cyl_intersect(t_env *env, t_raydir v, t_cyl cyl, int do_col)
{
	t_tracerez			ret_val;
	t_quadratic_coef	coef;
	t_vec3d				vxd;
	t_vec3d				kxd;
	t_quadratic_res		res;

	ret_val.no_hit = 0;
	vxd = cross_product(v.r, cyl.dir);
	kxd = cross_product(subtract(v.o, cyl.org), cyl.dir);
	coef.a = dot_product(vxd, vxd);
	coef.b = 2 * dot_product(kxd, vxd);
	coef.c = dot_product(kxd, kxd) - cyl.rad * cyl.rad;
	res = solve_quadratic(coef);
	if (!res.count)
		ret_val.no_hit = 1;
	else
	{
		ret_val.z_value = min_abs(res.x1, res.x2);
		if (do_col)
			ret_val.color = compute_cyl_col(env, v.r, cyl, ret_val.z_value);
	}
	return (ret_val);
}
