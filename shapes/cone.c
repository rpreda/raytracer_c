/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cone.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpreda <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/08 16:46:59 by rpreda            #+#    #+#             */
/*   Updated: 2017/12/08 16:53:37 by rpreda           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

t_color		compute_cone_col(t_env *env, t_vec3d ray, t_cone cone, double t)
{
	t_vec3d phit;
	t_vec3d normal;
	double	scale_factor;
	double	m;

	phit = scale(ray, t);
	m = dot_product(ray, scale(cone.axis, t)) +
		dot_product(subtract(env->origin, cone.vtx), cone.axis);
	scale_factor = (1 + cone.tg * cone.tg) * m;
	normal = subtract(subtract(phit, cone.vtx),
		scale(cone.axis, scale_factor));
	normal = normalize(normal);
	return (ph_light(env, pack(normal, phit), cone.color, ray));
}

t_tracerez	cone_intersect(t_env *env, t_raydir v, t_cone cn, int do_col)
{
	t_tracerez			ret_val;
	t_quadratic_coef	coef;
	t_quadratic_res		res;
	double				tg;
	double				ax_ray;

	ret_val.no_hit = 1;
	tg = (1 + cn.tg * cn.tg);
	ax_ray = dot_product(cn.axis, v.r);
	coef.a = dot_product(v.r, v.r) - tg * powf(ax_ray, 2);
	coef.b = 2 * (dot_product(v.r, subtract(v.o, cn.vtx)) -
			tg * ax_ray * dot_product(subtract(v.o, cn.vtx), cn.axis));
	coef.c = dot_product(subtract(v.o, cn.vtx),
		subtract(v.o, cn.vtx)) -
		tg * powf(dot_product(subtract(v.o, cn.vtx),
		cn.axis), 2);
	res = solve_quadratic(coef);
	if (res.count)
	{
		ret_val.no_hit = 0;
		ret_val.z_value = min_abs(res.x1, res.x2);
		if (do_col)
			ret_val.color = compute_cone_col(env, v.r, cn, ret_val.z_value);
	}
	return (ret_val);
}
