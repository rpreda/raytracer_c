/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   spheres.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpreda <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/08 17:01:07 by rpreda            #+#    #+#             */
/*   Updated: 2017/12/08 17:01:08 by rpreda           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

t_color		compute_sph_col(t_env *env, t_vec3d ray,
	t_sphere sp, double t)
{
	t_vec3d phit;
	t_vec3d normal;

	phit = add(env->origin, scale(ray, t));
	normal = normalize(subtract(phit, sp.center));
	return (ph_light(env, pack(normal, phit), sp.color, ray));
}

t_tracerez	sphere_intersect(t_env *env, t_raydir v, t_sphere sp, int do_col)
{
	t_tracerez			ret_val;
	t_quadratic_coef	coef;
	t_vec3d				l;
	t_quadratic_res		res;

	ret_val.no_hit = 0;
	l = subtract(v.o, sp.center);
	coef.a = dot_product(v.r, v.r);
	coef.b = 2 * dot_product(v.r, l);
	coef.c = dot_product(l, l) - sp.radius * sp.radius;
	res = solve_quadratic(coef);
	if (!res.count)
		ret_val.no_hit = 1;
	else
		ret_val.z_value = min_abs(res.x1, res.x2);
	if (do_col)
		ret_val.color = compute_sph_col(env, v.r, sp, ret_val.z_value);
	return (ret_val);
}
