/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   loops.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpreda <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/08 16:18:21 by rpreda            #+#    #+#             */
/*   Updated: 2017/12/08 17:02:04 by rpreda           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

t_tracerez	loop_spheres(t_env *env, t_raydir v, int col)
{
	t_tracerez	rez;
	t_tracerez	temp;
	int			i;

	init_loop(&i, &rez);
	while (i < env->spheres_count)
	{
		if (col)
			env->spheres[i].ignore = 1;
		if (col || !env->spheres[i].ignore)
		{
			temp = sphere_intersect(env, v, env->spheres[i], col);
			if (condition(temp, rez))
				rez = temp;
		}
		if (col)
			env->spheres[i].ignore = 0;
		i++;
	}
	return (rez);
}

t_tracerez	loop_cyls(t_env *env, t_raydir v, t_tracerez init, int col)
{
	t_tracerez	rez;
	t_tracerez	temp;
	int			i;

	init_loop(&i, &rez);
	if (!init.no_hit)
		rez = init;
	while (i < env->cyls_count)
	{
		if (col)
			env->cyls[i].ignore = 1;
		if (col || !env->cyls[i].ignore)
		{
			temp = cyl_intersect(env, v, env->cyls[i], col);
			if (condition(temp, rez))
				rez = temp;
		}
		if (col)
			env->cyls[i].ignore = 0;
		i++;
	}
	return (rez);
}

t_tracerez	loop_planes(t_env *env, t_raydir v, t_tracerez init, int col)
{
	t_tracerez	rez;
	t_tracerez	temp;
	int			i;

	init_loop(&i, &rez);
	if (!init.no_hit)
		rez = init;
	while (i < env->planes_count)
	{
		if (col)
			env->planes[i].ignore = 1;
		if (col || !env->planes[i].ignore)
		{
			temp = plane_intersect(env, v, env->planes[i], col);
			if (condition(temp, rez))
				rez = temp;
		}
		if (col)
			env->planes[i].ignore = 0;
		i++;
	}
	return (rez);
}

t_tracerez	loop_cones(t_env *env, t_raydir v, t_tracerez init, int col)
{
	t_tracerez	rez;
	t_tracerez	temp;
	int			i;

	init_loop(&i, &rez);
	if (!init.no_hit)
		rez = init;
	while (i < env->cones_count)
	{
		if (col)
			env->cones[i].ignore = 1;
		if (col || !env->cones[i].ignore)
		{
			temp = cone_intersect(env, v, env->cones[i], col);
			if (condition(temp, rez))
				rez = temp;
		}
		if (col)
			env->cones[i].ignore = 0;
		i++;
	}
	return (rez);
}

t_tracerez	loop(t_env *env, t_vec3d ray, t_vec3d orig, int do_col)
{
	t_tracerez res;

	res = loop_spheres(env, pack2(ray, orig), do_col);
	res = loop_cyls(env, pack2(ray, orig), res, do_col);
	res = loop_planes(env, pack2(ray, orig), res, do_col);
	res = loop_cones(env, pack2(ray, orig), res, do_col);
	return (res);
}
