/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   loops_aux.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpreda <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/08 16:09:52 by rpreda            #+#    #+#             */
/*   Updated: 2017/12/08 16:09:59 by rpreda           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

void	init_loop(int *i, t_tracerez *rez)
{
	*i = 0;
	rez->no_hit = 1;
	rez->z_value = 100000000;
}

int		condition(t_tracerez temp, t_tracerez rez)
{
	return (!temp.no_hit && fabs(temp.z_value) < fabs(rez.z_value));
}
