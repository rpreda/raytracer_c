/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   define_env.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpreda <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/08 15:04:20 by rpreda            #+#    #+#             */
/*   Updated: 2017/12/08 15:05:59 by rpreda           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

void	define_environment1(t_env *env)
{
	env->spheres_count = 3;
	env->spheres = (t_sphere *)malloc(sizeof(t_sphere) * env->spheres_count);
	env->spheres[0].center.x = -1;
	env->spheres[0].center.y = 0;
	env->spheres[0].center.z = -4;
	env->spheres[0].radius = 0.5;
	env->spheres[0].color.r = 1;
	env->spheres[0].color.g = 0;
	env->spheres[0].color.b = 0;
	env->spheres[1].center.x = -2;
	env->spheres[1].center.y = 1;
	env->spheres[1].center.z = -5;
	env->spheres[1].radius = 0.5;
	env->spheres[1].color.r = 0;
	env->spheres[1].color.g = 1;
	env->spheres[1].color.b = 1;
}

void	define_environment2(t_env *env)
{
	env->spheres[2].center.x = 1;
	env->spheres[2].center.y = 0;
	env->spheres[2].center.z = -4;
	env->spheres[2].radius = 0.6;
	env->spheres[2].color.r = 0;
	env->spheres[2].color.g = 1;
	env->spheres[2].color.b = 0;
	env->lights_count = 1;
	env->lights = (t_light *)malloc(sizeof(t_light) * env->lights_count);
	env->lights[0].position.x = 0;
	env->lights[0].position.y = 5;
	env->lights[0].position.z = 10;
	env->lights[0].intensity.r = 1;
	env->lights[0].intensity.g = 1;
	env->lights[0].intensity.b = 1;
}

void	define_environment3(t_env *env)
{
	env->cyls_count = 2;
	env->cyls = (t_cyl *)malloc(sizeof(t_cyl) * env->cyls_count);
	env->cyls[0].rad = 0.5;
	env->cyls[0].color = env->spheres[2].color;
	env->cyls[0].dir.x = 0.3;
	env->cyls[0].dir.y = 1;
	env->cyls[0].dir.z = -0.4;
	env->cyls[0].org.x = 0;
	env->cyls[0].org.y = 0;
	env->cyls[0].org.z = -5;
	env->cyls[1].rad = 0.5;
	env->cyls[1].color.r = 1;
	env->cyls[1].color.g = 0;
	env->cyls[1].color.b = 1;
	env->cyls[1].dir.x = -0.3;
	env->cyls[1].dir.y = 1;
	env->cyls[1].dir.z = -0.4;
	env->cyls[1].org.x = 2;
	env->cyls[1].org.y = 0;
	env->cyls[1].org.z = -5;
}

void	define_environment4(t_env *env)
{
	env->planes_count = 1;
	env->planes = (t_plane *)malloc(sizeof(t_plane) * env->planes_count);
	env->planes[0].p.x = 0;
	env->planes[0].p.y = -1;
	env->planes[0].p.z = 0;
	env->planes[0].normal.x = -0.1;
	env->planes[0].normal.y = -1;
	env->planes[0].normal.z = 0;
	env->planes[0].color.r = 0.5;
	env->planes[0].color.g = 0.5;
	env->planes[0].color.b = 0.5;
}

void	define_environment(t_env *env)
{
	env->origin.x = 0;
	env->origin.y = 0;
	env->origin.z = 0;
	define_environment1(env);
	define_environment2(env);
	define_environment3(env);
	define_environment4(env);
	env->cones_count = 1;
	env->cones = (t_cone *)malloc(sizeof(t_cone) * env->cones_count);
	env->cones[0].color.r = 0.5;
	env->cones[0].color.g = 0.1;
	env->cones[0].color.b = 0.9;
	env->cones[0].tg = 0.37735;
	env->cones[0].vtx.x = -0.2;
	env->cones[0].vtx.y = 0;
	env->cones[0].vtx.z = -3;
	env->cones[0].axis.x = 0;
	env->cones[0].axis.y = 1;
	env->cones[0].axis.z = 0;
}
