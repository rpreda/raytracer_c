/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpreda <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/08 17:11:58 by rpreda            #+#    #+#             */
/*   Updated: 2017/12/08 17:14:19 by rpreda           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef H_RT_H
# define H_RT_H
# define WIDTH 1280
# define HEIGHT 720
# define M_PI 3.14159265
# define K_DIFF 1
# define K_SPEC 0.9
# define K_AMBI 1
# define AMBIENT 0.1
# include "mlx.h"
# include <stdlib.h>
# include <math.h>

typedef struct	s_color_simplified
{
	float		r;
	float		g;
	float		b;
}				t_color;

typedef struct	s_vec3d
{
	double		x;
	double		y;
	double		z;
}				t_vec3d;

typedef struct	s_sphere
{
	float		radius;
	t_vec3d		center;
	t_color		color;
	int			ignore;
}				t_sphere;

typedef struct	s_cyl
{
	float		rad;
	t_color		color;
	t_vec3d		dir;
	t_vec3d		org;
	int			ignore;
}				t_cyl;

typedef struct	s_quadratic_coef
{
	double		a;
	double		b;
	double		c;
}				t_quadratic_coef;

typedef	struct	s_quadratic_res
{
	double		x1;
	double		x2;
	int			count;
}				t_quadratic_res;

typedef	struct	s_tracerez
{
	char		no_hit;
	t_color		color;
	double		z_value;
}				t_tracerez;

typedef struct	s_light
{
	t_vec3d		position;
	t_color		intensity;
}				t_light;

typedef struct	s_plane
{
	t_vec3d		p;
	t_vec3d		normal;
	t_color		color;
	int			ignore;
}				t_plane;

typedef struct	s_cone
{
	t_vec3d		vtx;
	t_vec3d		axis;
	double		tg;
	t_color		color;
	int			ignore;
}				t_cone;

typedef struct	s_twovec
{
	t_vec3d		normal;
	t_vec3d		phit;
}				t_twovec;

typedef struct	s_raydir
{
	t_vec3d		r;
	t_vec3d		o;
}				t_raydir;

typedef struct	s_env {
	void		*mlx;
	void		*win;
	void		*image;
	char		*img;
	int			bpp;
	int			ln_size;
	int			endian;
	t_vec3d		origin;
	t_sphere	*spheres;
	int			spheres_count;
	t_light		*lights;
	int			lights_count;
	t_cyl		*cyls;
	int			cyls_count;
	t_plane		*planes;
	int			planes_count;
	t_cone		*cones;
	int			cones_count;
}				t_env;

double			magnitude(t_vec3d a);
t_vec3d			normalize(t_vec3d a);
double			dot_product(t_vec3d a, t_vec3d b);
t_vec3d			cross_product(t_vec3d a, t_vec3d b);
t_vec3d			subtract(t_vec3d a, t_vec3d b);
t_vec3d			add(t_vec3d a, t_vec3d b);
t_vec3d			scale(t_vec3d a, double num);
t_quadratic_res	solve_quadratic(t_quadratic_coef coef);
float			clamp(float min, float val, float max);
float			max(float a, float b);
float			min(float a, float b);
float			min_abs(float a, float b);
t_tracerez		sphere_intersect(t_env *e, t_raydir a, t_sphere s, int i);
t_tracerez		cyl_intersect(t_env *d, t_raydir c, t_cyl b, int a);
t_tracerez		plane_intersect(t_env *a, t_raydir b, t_plane c, int d);
t_tracerez		cone_intersect(t_env *d, t_raydir c, t_cone b, int a);
t_color			ph_light(t_env *e, t_twovec f, t_color c, t_vec3d r);
int				is_shadowed(t_env *e, t_vec3d p, t_vec3d d);
t_tracerez		loop(t_env *env, t_vec3d ray, t_vec3d orig, int k);
void			init_loop(int *i, t_tracerez *rez);
int				condition(t_tracerez temp, t_tracerez rez);
void			define_environment(t_env *env);
t_twovec		pack(t_vec3d ka, t_vec3d ppa);
void			put_pixel(t_env *fu, t_color k, int pa, int rams);
t_raydir		pack2(t_vec3d ray, t_vec3d orig);
double			magnitude(t_vec3d a);
#endif
